import React, { Component } from 'react';
import './App.css';
import JoyStick from 'react-joystick';
import axios from 'axios'

var Api_Url = 'http://192.168.0.100/post-json';


//import habijabi from "react-shake-effect"
var b = 0;
var x, y;



const joyOptions = {

  mode: 'semi',
  catchDistance: 150,
  color: 'red',
  size: '500px'

}


const containerStyle1 = {
  position: 'relative',
  height: '250px',
  width: '100%',

  // background:'(#77A1D3)'
  // background: 'linear-gradient(to right, #E684AE, #79CBCA, #77A1D3)'
  background: 'linear-gradient(to right, #089379, #089379)'
}

class JoyWrapper extends Component {
  constructor() {
    super();
    this.managerListener = this.managerListener_1.bind(this);
  }


//first stick
  managerListener_1(manager) {
    manager.on('move', (e, stick) => {

      x = stick.distance * Math.cos(stick.angle.degree * (Math.PI / 180))/50;
      console.log('x = ', x);

      if (b === 1) {
        axios.post('http://192.168.0.100/post-json', { "JL": x })
       //axios.post(Api_Url, { "JL": x })
        .then(
          (resp) =>{
            console.log(resp);
          }
        ).catch((err) =>{
          console.log(err);
        });
        console.log("start posting x");
      }

      if (b === 0) {
        console.log("post request is off for x");
      }
    })

    manager.on('end', () => {
      if (b === 1) { 
        axios.post('http://192.168.0.100/post-json', { "JL": 0 })
        //axios.post(Api_Url, { "JL": 0 })
      .then(
        (resp) =>{
          console.log(resp);
        }
      ).catch((err) =>{
        console.log(err);
      }); }
    })
  }




  managerListener_2(manager) {
    manager.on('move', (e, stick) => {

      y = stick.distance * Math.sin(stick.angle.degree * (Math.PI / 180))/50;
      console.log('y = ', y)

      if (b === 1) //post when button is on post_y 
      {
        axios.post('http://192.168.0.100/post-json', { "JR": y })
        //axios.post(Api_Url, { "JR": y })
        .then(
          (resp) =>{
            console.log(resp);
          }
        ).catch((err) =>{
          console.log(err);
        });
        console.log("start posting y");
      }
      if (b === 0) { console.log("post request is off of y") }
    })

    manager.on('end', () => {
      if (b === 1) { 
        axios.post('http://192.168.0.100/post-json', { "JR": 0 })
        //axios.post(Api_Url, { "ForwardBackwardVal": 0 })
        .then(
          (resp) =>{
            console.log(resp);
          }
        ).catch((err) =>{
          console.log(err);
        })
      ;
    }
    })
  }




  render() {
    const { classes } = this.props;
    return (
      <div>

        <h1>VAL-JOYSTICK</h1>

        <h3>LEFT_RIGHT</h3>
        <JoyStick joyOptions={joyOptions} containerStyle={containerStyle1} managerListener={this.managerListener_1}></JoyStick>
        <h3>UP DOWN</h3>
        <JoyStick joyOptions={joyOptions} containerStyle={containerStyle1} managerListener={this.managerListener_2} />

        <button onClick={Start}>start</button>
        <button onClick={Stop}>stop</button>



      </div>


    )
  }
}

export default JoyWrapper;



function Start() { b = 1; }
function Stop() { b = 0 }